# Gardener Pid Controller #

Gardener Proportional Integral Derivative (PID) controller: Performs gradual watering using Soil Moisture % readings.

This Proof-of-Concept project was used to simulate PID waterings and soil humidity% feedback for the later implementation in .Net Core 2.0 on Windows IoT core.

![alternativetext](wateringsPid.png)

## Techniques & algorithms

- Control Theory
- Proportional Integral Derivative (PID) controller

## SDKs & Libraries

- random
- matplotlib

