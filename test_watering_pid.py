#!/usr/bin/python
#
# This file is part of IvPID.
# Copyright (C) 2015 Ivmech Mechatronics Ltd. <bilgi@ivmech.com>
#
# IvPID is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IvPID is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#title           :test_watering_pid.py
#description     :python pid controller test
#author          :Caner Durmusoglu
#date            :20151218
#version         :0.1
#notes           :
#python_version  :2.7
#dependencies    : matplotlib, numpy, scipy
#==============================================================================

import PID
import matplotlib.pyplot as plt
import random

def get_simulated_feedback(sensor_feedback, pid_output, i):
    return ((1 - random.random() / 8) * sensor_feedback + (1 - random.random()/8) * pid_output
            - ((1 - random.random()/8)/(i + 2))) * 0.35

def show_test():

    p = 1.2
    integral = 0.9
    d = 0.4
    # p = 1.2
    # integral = 0.6
    # d = 0.29
    pid = PID.PID(p, integral, d)

    # pid.setPoints(desired_moisture_min, desired_moisture_max)
    desired_moisture = 26
    tolerance_percent = 0.05
    pid.setPoint(desired_moisture, tolerance_percent)
    desired_moisture_min = desired_moisture * (1 - tolerance_percent)
    desired_moisture_max = desired_moisture * (1 + tolerance_percent)

    init_sensor_value = 13
    totalTimeSteps = 30
    sensor_feedback = init_sensor_value

    sensor_readings = []
    timeSteps = []
    outputs = []
    for i in range(0, totalTimeSteps):
        pid_output = pid.update(sensor_feedback)
        sensor_readings.append(sensor_feedback)
        outputs.append(pid_output)
        timeSteps.append(i)
        sensor_feedback = get_simulated_feedback(sensor_feedback, pid_output, i)  # simulated plant response
        if pid_output == 0:
            break # Stop watering if reached the desired moisture range

    plt.figure(1, figsize=(8, 6))  # Resolution 800 x 600
    plt.subplot(2, 1, 1)
    plt.title('Soil Moisture% Readings - Target {} (with {}% tolerance)'
          .format(desired_moisture, tolerance_percent * 100, round(desired_moisture_min, 2), round(desired_moisture_max, 2), 3))
    plt.xlabel('time step')
    plt.ylabel('Soil Moisture (%)')
    plt.hlines(y=desired_moisture_min, xmin=0, xmax=len(timeSteps), color="gray", linestyles='dashed')
    plt.hlines(y=desired_moisture_max, xmin=0, xmax=len(timeSteps), color="black", linestyles='dashed')
    plt.legend(["Set Point: Min", "Set Point: Max"])
    # plt.scatter(timeSteps, sensor_readings)
    plt.plot(timeSteps, sensor_readings,'g-')
    plt.grid(True)
    plt.subplot(2, 1, 2)
    plt.title('Waterings: {} - Total water: {} mL - PID(P:{},I:{},D:{})'
          .format(len(outputs) - 1, round(sum(outputs), 2), round(p, 2), round(integral, 2),round(d, 2)))
    plt.plot(timeSteps, outputs)
    plt.scatter(timeSteps, outputs)
    plt.xlabel('time step')
    plt.ylabel('Water Deposit (mL)')
    plt.grid(True)
    plt.show()


if __name__ == "__main__":
    show_test()