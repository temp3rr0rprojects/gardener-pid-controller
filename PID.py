# The recipe gives simple implementation of a Discrete Proportional-Integral-Derivative (PID) controller.
# PID controller gives output value for error between desired reference input and measurement
# feedback to minimize error value.
# More information: http://en.wikipedia.org/wiki/PID_controller
# cnr437@gmail.com

class PID:
	"""Discrete PID control	"""

	def __init__(self, p=2.0, i=0.0, d=1.0, derivator=0, integrator=0, integrator_max=500, integrator_min=-500):
		self.Kp = p
		self.Ki = i
		self.Kd = d
		self.Derivator = derivator
		self.Integrator = integrator
		self.Integrator_max = integrator_max
		self.Integrator_min = integrator_min

		self.set_min_point = 0.0
		self.set_max_point = 0.0

	def update(self, current_value):
		"""Calculate PID output value for given reference input and feedback"""

		self.error = (self.set_min_point + self.set_max_point)/2 - current_value

		PID = 0
		if not self.set_min_point < current_value < self.set_max_point:
			self.P_value = self.Kp * self.error
			self.D_value = self.Kd * (self.error - self.Derivator)
			self.Derivator = self.error

			self.Integrator = self.Integrator + self.error

			if self.Integrator > self.Integrator_max:
				self.Integrator = self.Integrator_max
			elif self.Integrator < self.Integrator_min:
				self.Integrator = self.Integrator_min

			self.I_value = self.Integrator * self.Ki

			PID = self.P_value + self.I_value + self.D_value

		return PID

	def setTolerancePercent(self, tolerance_percent):
		self.tolerance_percent = tolerance_percent

	def setPoint(self, set_point, tolerance_percent):
		"""Initilize the setpoint of PID"""
		self.set_min_point = set_point * (1 - tolerance_percent)
		self.set_max_point = set_point * (1 + tolerance_percent)
		self.Integrator = 0
		self.Derivator = 0

	def setPoints(self, set_min_point, set_max_point):
		"""Initilize the setpoint of PID"""
		self.set_min_point = set_min_point
		self.set_max_point = set_max_point
		self.Integrator = 0
		self.Derivator = 0

	def setIntegrator(self, Integrator):
		self.Integrator = Integrator

	def setDerivator(self, Derivator):
		self.Derivator = Derivator

	def setKp(self, P):
		self.Kp = P

	def setKi(self, I):
		self.Ki = I

	def setKd(self, D):
		self.Kd = D

	def getError(self):
		return self.error

	def getIntegrator(self):
		return self.Integrator

	def getDerivator(self):
		return self.Derivator
